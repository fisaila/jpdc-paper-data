# README #

This respository contains the data used in the paper "Cooperative Collective I/O: Speeding up Parallel Producer-Consumer Patterns" 
by Florin Isaila, Jesus Carretero, Robert Bindar, Georgiana Dolocan and Tom Peterka.


### Files ###

* fig8_9_vpicio_vorpalio.xlsx
	* Figure 8: VPICIO results for the nine configuration cases: the speedup  of CCIO over TPIO for aggregated write and read operations, 
	the aggregate write throughput for CCIO and TPIO (in log scale), and the aggregate read throughput for CCIO and TPIO (in log scale).
	* Figure 9: VORPALIO results for the nine configuration cases: the speedup  of CCIO over TPIO for aggregated write and read operations, 
	the aggregate write throughput for CCIO and TPIO (in log scale), and the aggregate read throughput for CCIO and TPIO (in log scale).
* fig10_decouple.xlsx
	* Figure 10: Performance comparison between MPI-coupled producer-consumers and Bufferflow-decoupled producer consumers.
* fig11_12_memory_footprint_comparison.xlsm
	* Figure 11: Memory footprint for adaptive, on-demand, and static allocation scheduling.
	* Figure 12: Average number of buffers allocated by adaptive, on-demand, and static allocation scheduling.
* fig13_14_15_16_memory_footprint_watermarks.xlsm
	* Figure 13: Memory footprint for adaptive allocation scheduling with 1% low watermark and 100% high watermark
	* Figure 14: Memory footprint for adaptive allocation scheduling with 10% low watermark and 90% high watermark
	* Figure 15: Memory footprint for adaptive allocation scheduling with 20% low watermark and 80% high watermark
	* Figure 16: Memory footprint for adaptive allocation scheduling with 40% low watermark and 60% high watermark